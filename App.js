import React from "react";

import { Provider } from "react-redux";

import { SafeAreaProvider } from "react-native-safe-area-context";

import Store from "./src/store";
import RootNavigator from "./src/navigations";
import { LogBox } from "react-native";

LogBox.ignoreAllLogs("Seems like you're using an old API with gesture components, check out new Gestures system!");

const App = () => {
  return (
    <Provider store={Store}>
      <SafeAreaProvider>
        <RootNavigator />
      </SafeAreaProvider>
    </Provider>
  );
};

export default App;
