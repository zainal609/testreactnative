export const dataLogin = {
  SET_DATA_LOGIN: "SET_DATA_LOGIN",
  SET_USER_LOGIN: "SET_USER_LOGIN",
};

export const dataProfile = {
  SET_DATA_PROFILE: "SET_DATA_PROFILE",
};
