import { dataLogin } from "../actionType";

const initialState = {
  userLogin: false,
};

const dataLoginReducers = (state = initialState, action) => {
  switch (action.type) {
    // case dataLogin.SET_DATA_LOGIN:
    //   return { ...state, ...action.payload };
    case dataLogin.SET_USER_LOGIN:
      return { ...state, ...action.payload };

    default:
      return { ...state };
  }
};

export default dataLoginReducers;
