import { dataLogin } from "../actionType";

// export const setDataLogin = (data) => {
//   return {
//     type: dataLogin.SET_DATA_LOGIN,
//     payload: data,
//   };
// };

export const setUserLogin = (data) => {
  return {
    type: dataLogin.SET_USER_LOGIN,
    payload: data,
  };
};
