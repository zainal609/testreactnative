import React from "react";
import { createStore, combineReducers, applyMiddleware } from "redux";
import thunk from "redux-thunk";

import DataLoginReducers from "./reducers/dataLoginReducers";

const reducers = combineReducers({
  DataLoginReducers,
});

const store = createStore(reducers, applyMiddleware(thunk));

export default store;
