import React, { useState, useEffect, useCallback } from "react";
import { View, Image, Platform } from "react-native";
import { useTheme, Text, Badge } from "react-native-paper";

import { createStackNavigator } from "@react-navigation/stack";
import UserScreen from "../screens/user";
import DetailUserScreen from "../screens/detailUser";
import CartScreen from "../screens/cart";
import DetailCart from "../screens/detailCart";

const Stack = createStackNavigator();

const AppNavigator = () => {
  return (
    <Stack.Navigator>
      <Stack.Screen name="User" component={UserScreen} options={{ headerShown: false }} />
      <Stack.Screen name="DetailUser" component={DetailUserScreen} options={{ headerShown: false }} />
      <Stack.Screen name="Cart" component={CartScreen} options={{ headerShown: false }} />
      <Stack.Screen name="DetailCart" component={DetailCart} options={{ headerShown: false }} />
    </Stack.Navigator>
  );
};

export default AppNavigator;
