import React from "react";

import { NavigationContainer } from "@react-navigation/native";
import { createStackNavigator } from "@react-navigation/stack";

import { useSelector } from "react-redux";
import AppNavigator from "./app-navigator";
import AuthNavigator from "./auth-navigator";

const Stack = createStackNavigator();

const RootNavigator = () => {
  const { DataLoginReducers } = useSelector((selector) => selector);

  console.log(DataLoginReducers);

  return (
    <NavigationContainer>
      <Stack.Navigator>
        {DataLoginReducers?.userLogin ? (
          <Stack.Screen name="App" component={AppNavigator} options={{ headerShown: false }} />
        ) : (
          <Stack.Screen name="Auth" component={AuthNavigator} options={{ headerShown: false }} />
        )}
      </Stack.Navigator>
    </NavigationContainer>
  );
};

export default RootNavigator;
