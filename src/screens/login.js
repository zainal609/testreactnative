import React, { useRef, useState } from "react";

import { StyleSheet, View, ScrollView, StatusBar } from "react-native";
import { IconButton, Caption, Button, Text } from "react-native-paper";

import { SafeAreaView } from "react-native-safe-area-context";

import { useFormik } from "formik";
import * as Yup from "yup";

import axios from "axios";

import { useDispatch } from "react-redux";
import TextInputOnly from "../components/textInputOnly";
import { setUserLogin } from "../store/actions/dataLoginAction";

import { useNavigation } from "@react-navigation/native";

const LoginScreen = () => {
  const passwordInputRef = useRef();
  const disptach = useDispatch();
  const navigation = useNavigation();

  const [errorMessage, setErrorMessage] = useState("");

  const inputTheme = {
    colors: {
      background: "white",
    },
  };

  const handleSubmitLogin = async (input) => {
    try {
      const response = await axios.post("https://fakestoreapi.com/auth/login", input);
      if (response?.data) {
        console.log(response.data);
        disptach(setUserLogin({ userLogin: true }));
        navigation.navigate("User");
      } else {
        setErrorMessage(response);
      }
    } catch (error) {
      setErrorMessage(error);
    }
  };

  const formik = useFormik({
    initialValues: {
      username: "",
      password: "",
    },
    validationSchema: Yup.object({
      username: Yup.string().trim().required(),
      password: Yup.string().trim().required(),
    }),
    onSubmit: async (values) => {
      await handleSubmitLogin(values);
    },
  });

  const handleSubmitEditing = () => passwordInputRef.current.focus();

  const isDisabled =
    !formik.values.username ||
    !formik.values.password ||
    formik.errors.username ||
    formik.errors.password ||
    formik.isSubmitting;

  return (
    <SafeAreaView
      style={{
        ...styles.container,
      }}>
      <StatusBar barStyle="dark-content" translucent backgroundColor="transparent" />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.authCont}>
          <Text style={{ color: "black", fontSize: 18 }}>Login</Text>
          <View style={styles.formCont}>
            <View style={{ flexDirection: "row" }}>
              <Text style={{ color: "black", fontWeight: "bold" }}>
                Username<Text style={{ color: "red" }}>*</Text>
              </Text>
            </View>
            <TextInputOnly
              inputProps={{
                name: "username",
                onChangeText: formik.handleChange("username"),
                onBlur: formik.handleBlur("username"),
                value: formik.values.username,
                mode: "outlined",
                autoCompleteType: "username",
                autoCapitalize: "none",
                returnKeyType: "next",
                // placeholder: translations["email.placeholder"],
                onSubmitEditing: handleSubmitEditing,
                theme: inputTheme,
                style: { fontSize: 14 },
                dense: true,
              }}
            />
            {formik.errors.username && formik.touched.username && (
              <Caption style={{ color: "red" }}>{formik.errors.username}</Caption>
            )}

            <View style={{ flexDirection: "row", marginTop: 20 }}>
              <View style={{ flexDirection: "row" }}>
                <Text style={{ color: "black", fontWeight: "bold" }}>
                  Password<Text style={{ color: "red" }}>*</Text>
                </Text>
              </View>
            </View>
            <TextInputOnly
              mode="password"
              inputProps={{
                ref: passwordInputRef,
                name: "password",
                onChangeText: formik.handleChange("password"),
                onBlur: formik.handleBlur("password"),
                value: formik.values.password,
                mode: "outlined",
                autoCapitalize: "none",
                returnKeyType: "done",
                // placeholder: translations["password.placeholder"],
                onSubmitEditing: formik.handleSubmit,
                theme: inputTheme,
                style: { fontSize: 14 },
                dense: true,
              }}
              iconProps={{
                // color: Colors.GREY_MEDIUM,
                size: 24,
              }}
            />
            {formik.errors.password && formik.touched.password && (
              <Caption style={{ color: "red" }}>{formik.errors.password}</Caption>
            )}

            {errorMessage !== "" && <Caption style={{ color: "red" }}>{errorMessage}</Caption>}

            <Button
              mode="contained"
              style={{
                backgroundColor: isDisabled ? "grey" : "green",
                marginTop: 32,
              }}
              labelStyle={{
                color: "white",
                fontSize: 16,
                letterSpacing: 0,
              }}
              contentStyle={{ height: 44 }}
              disabled={isDisabled}
              loading={formik.isSubmitting}
              onPress={formik.handleSubmit}>
              OK
            </Button>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default LoginScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  imgCont: {
    flex: 2,
    alignItems: "center",
    justifyContent: "center",
    marginTop: 50,
  },
  authCont: {
    flex: 3,
    paddingTop: 60,
    paddingHorizontal: 20,
  },
  formCont: {
    marginTop: 32,
  },
});
