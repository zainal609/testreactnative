import React, { useEffect, useRef, useState } from "react";

import { StyleSheet, View, ScrollView, TouchableOpacity, StatusBar, FlatList } from "react-native";
import { Text } from "react-native-paper";

import { SafeAreaView } from "react-native-safe-area-context";

import axios from "axios";

import { useDispatch } from "react-redux";

import { useNavigation } from "@react-navigation/native";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

const UserScreen = () => {
  const navigation = useNavigation();

  const [dataUsers, setDataUsers] = useState([]);

  const handleGetUsers = async () => {
    try {
      const response = await axios.get("https://fakestoreapi.com/users");

      setDataUsers(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    handleGetUsers();
  }, []);

  return (
    <SafeAreaView
      style={{
        ...styles.container,
      }}>
      <StatusBar barStyle="dark-content" translucent backgroundColor="transparent" />

      <ScrollView showsVerticalScrollIndicator={false}>
        <View style={styles.authCont}>
          <TouchableOpacity
            onPress={() => {
              navigation.navigate("Cart");
            }}
            style={{ padding: 6, backgroundColor: "orange", width: 80, borderRadius: 6, alignItems: "center" }}>
            <Text style={{ color: "white" }}>Cart</Text>
          </TouchableOpacity>
          <FlatList
            data={dataUsers}
            refreshing={false}
            keyExtractor={(item, index) => item?.id ?? index.toString()}
            renderItem={({ item, index }) => {
              if (!item) {
                return null;
              }

              return (
                <TouchableOpacity
                  key={item?.id}
                  onPress={() => {
                    navigation.navigate("DetailUser", { data: item });
                  }}
                  style={{
                    paddingVertical: 10,
                    borderBottomColor: "grey",
                    borderBottomWidth: 0.3,
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}>
                  <View>
                    <Text style={{ color: "black", fontSize: 16, fontWeight: "bold" }}>
                      {item?.name?.firstname}
                      {item?.name?.lastname}
                    </Text>
                    <Text style={{ color: "black" }}>{item?.email}</Text>
                  </View>
                  <MaterialIcons name="arrow-right" size={32} color="black" />
                </TouchableOpacity>
              );
            }}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default UserScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  authCont: {
    flex: 3,
    paddingTop: 16,
    paddingHorizontal: 20,
  },
});
