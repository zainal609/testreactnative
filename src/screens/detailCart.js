import React, { useEffect, useState } from "react";

import { StyleSheet, View, ScrollView, StatusBar } from "react-native";
import { IconButton, Text } from "react-native-paper";

import { SafeAreaView } from "react-native-safe-area-context";

import axios from "axios";

const DetailCart = ({ navigation, route }) => {
  const { id } = route.params;

  const [dataCart, setDataCart] = useState([]);

  const handleGetDetailCart = async () => {
    try {
      const response = await axios.get(`https://fakestoreapi.com/carts/${id}`);

      setDataCart(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    handleGetDetailCart();
  }, []);

  return (
    <SafeAreaView
      style={{
        ...styles.container,
      }}>
      <StatusBar barStyle="dark-content" translucent backgroundColor="transparent" />

      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}>
          <IconButton icon="chevron-left" size={30} onPress={navigation.goBack} />
        </View>
        <View style={styles.authCont}>
          <View
            style={{
              paddingVertical: 10,
              borderBottomColor: "grey",
              borderBottomWidth: 0.3,
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
            }}>
            <View>
              <Text style={{ color: "black", fontSize: 16, fontWeight: "bold" }}>{dataCart?.date}</Text>
              {dataCart?.products.map((data, index) => (
                <View key={index}>
                  <Text style={{ color: "black" }}>Produk {data?.productId}</Text>
                  <Text style={{ color: "black" }}>Stok: {data?.quantity}</Text>
                </View>
              ))}
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default DetailCart;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  authCont: {
    flex: 3,
    paddingTop: 16,
    paddingHorizontal: 20,
  },
});
