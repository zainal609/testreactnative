import React, { useState, useEffect } from "react";
import { StyleSheet, View, ScrollView, TouchableOpacity, StatusBar, FlatList } from "react-native";
import { IconButton, Text } from "react-native-paper";

import { SafeAreaView } from "react-native-safe-area-context";

import axios from "axios";
import MaterialIcons from "react-native-vector-icons/MaterialIcons";

import moment from "moment";

import DateTimePickerModal from "react-native-modal-datetime-picker";

const CartScreen = ({ navigation }) => {
  const [dataCart, setDataCart] = useState([]);
  const [modalStartDate, setModalStartDate] = useState({});
  const [modalEndDate, setModalEndDate] = useState({});

  const handleGetCarts = async () => {
    try {
      const response = await axios.get(`https://fakestoreapi.com/carts/startdate=2019-12-10&enddate=2020-03-01`);
      console.log(response);

      setDataCart(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  const handleGetFilterCarts = async () => {
    // setDataCart([]);
    try {
      console.log(`https://fakestoreapi.com/carts/startdate=${modalStartDate.data}&enddate=${modalEndDate.data}`);
      const response = await axios.get(
        `https://fakestoreapi.com/carts/startdate=${modalStartDate.data}&enddate=${modalEndDate.data}`,
      );
      console.log(response);
      console.log("heyyyyyy");

      // setDataCart(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  useEffect(() => {
    handleGetCarts();
  }, []);

  return (
    <SafeAreaView
      style={{
        ...styles.container,
      }}>
      <StatusBar barStyle="dark-content" translucent backgroundColor="transparent" />

      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}>
          <IconButton icon="chevron-left" size={30} onPress={navigation.goBack} />
        </View>
        <View style={styles.authCont}>
          <View style={{ flexDirection: "row" }}>
            <TouchableOpacity
              onPress={() => {
                setModalStartDate({ status: true });
              }}
              style={{ padding: 6, backgroundColor: "orange", width: 100, borderRadius: 6, alignItems: "center" }}>
              <Text style={{ color: "white" }}>{modalStartDate?.data ?? "Start date"}</Text>
            </TouchableOpacity>
            <TouchableOpacity
              onPress={() => {
                setModalEndDate({ status: true });
              }}
              style={{
                padding: 6,
                backgroundColor: "orange",
                width: 100,
                marginLeft: 5,
                borderRadius: 6,
                alignItems: "center",
              }}>
              <Text style={{ color: "white" }}>{modalEndDate?.data ?? "End date"}</Text>
            </TouchableOpacity>

            <TouchableOpacity
              onPress={() => handleGetFilterCarts()}
              disabled={!modalStartDate.data && !modalEndDate.data}
              style={{
                padding: 6,
                backgroundColor: "blue",
                width: 100,
                marginLeft: 5,
                borderRadius: 6,
                alignItems: "center",
              }}>
              <Text style={{ color: "white" }}>OK</Text>
            </TouchableOpacity>
          </View>
          <FlatList
            data={dataCart}
            refreshing={false}
            keyExtractor={(item, index) => item?.id ?? index.toString()}
            renderItem={({ item, index }) => {
              if (!item) {
                return null;
              }

              return (
                <TouchableOpacity
                  key={index}
                  onPress={() => {
                    navigation.navigate("DetailCart", { id: item?.id });
                  }}
                  style={{
                    paddingVertical: 10,
                    borderBottomColor: "grey",
                    borderBottomWidth: 0.3,
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center",
                  }}>
                  <View>
                    <Text style={{ color: "black", fontWeight: "bold" }}>
                      {moment(item?.date).format("YYYY-MM-DD hh:mm:ss")}
                    </Text>
                    {item?.products.map((data) => (
                      <View key={data?.productId}>
                        <Text style={{ color: "black" }}>Produk {data?.productId}</Text>
                        <Text style={{ color: "black" }}>Stok: {data?.quantity}</Text>
                      </View>
                    ))}
                  </View>
                  <MaterialIcons name="arrow-right" size={32} color="black" />
                </TouchableOpacity>
              );
            }}
          />
        </View>
      </ScrollView>
      {modalStartDate && (
        <DateTimePickerModal
          isVisible={modalStartDate.status}
          mode="date"
          locale="id_ID"
          date={moment().toDate()}
          onConfirm={(date) => {
            setModalStartDate(() => ({ status: false, data: moment(date).format("YYYY-MM-DD") }));
          }}
          onCancel={() => setModalStartDate(false)}
        />
      )}
      {modalEndDate && (
        <DateTimePickerModal
          isVisible={modalEndDate.status}
          mode="date"
          locale="id_ID"
          date={moment().toDate()}
          onConfirm={(date) => {
            setModalEndDate(() => ({ status: false, data: moment(date).format("YYYY-MM-DD") }));
          }}
          onCancel={() => setModalEndDate(false)}
        />
      )}
    </SafeAreaView>
  );
};

export default CartScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  authCont: {
    flex: 3,
    paddingTop: 16,
    paddingHorizontal: 20,
  },
});
