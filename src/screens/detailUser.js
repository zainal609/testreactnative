import React from "react";

import { StyleSheet, View, ScrollView, StatusBar } from "react-native";
import { IconButton, Text } from "react-native-paper";

import { SafeAreaView } from "react-native-safe-area-context";

const DetailUserScreen = ({ route, navigation }) => {
  const { data } = route?.params;

  return (
    <SafeAreaView
      style={{
        ...styles.container,
      }}>
      <StatusBar barStyle="dark-content" translucent backgroundColor="transparent" />
      <ScrollView showsVerticalScrollIndicator={false}>
        <View
          style={{
            flexDirection: "row",
            justifyContent: "space-between",
            alignItems: "center",
          }}>
          <IconButton icon="chevron-left" size={30} onPress={navigation.goBack} />
        </View>
        <View style={styles.authCont}>
          <View
            style={{
              paddingVertical: 10,
              borderBottomColor: "grey",
              borderBottomWidth: 0.3,
              flexDirection: "row",
              justifyContent: "space-between",
              alignItems: "center",
            }}>
            <View>
              <Text style={{ color: "black", fontSize: 16, fontWeight: "bold" }}>
                {data?.name?.firstname}
                {data?.name?.lastname}
              </Text>
              <Text style={{ color: "black" }}>{data?.email}</Text>
              <Text style={{ color: "black" }}>Nomer Hp: {data?.phone}</Text>
              <Text style={{ color: "black" }}>
                Alamat: {data?.address?.street} {data?.address?.number} {data?.address?.city} {data?.address?.zipcode}
              </Text>
            </View>
          </View>
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default DetailUserScreen;

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  authCont: {
    flex: 3,
    paddingTop: 16,
    paddingHorizontal: 20,
  },
});
